export default function ({ $axios, $toast }) {

  $axios.defaults.withCredentials = true;

  $axios.onRequest(config => {
    console.log('Making request to ' + config.url)
  })

  $axios.onError(error => {

    const { message, exception, errors} = error.response.data

    if (error.response.status === 401) {
      window.localStorage.removeItem('user');
      return
    }

    if (errors !== undefined) {
      Object.keys(errors).forEach(function (item) {
        errors[item].forEach(function (errorMessage) {
          $toast.show({
            type: 'danger',
            title: message,
            message: errorMessage,
          })
        })
      });
    } else {
      $toast.show({
        type: 'danger',
        title: exception,
        message: message,
      })
    }

  })

}

