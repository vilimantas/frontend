import Auth from '/api/repositories/auth';
import Notification from '/modules/notification/api/notification';

export default ({ $axios }, inject) => {

  const repositories = {
    auth: Auth($axios),
    notification: Notification($axios)
  };

  inject('repository', repositories);
};
