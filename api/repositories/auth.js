const END_POINT = 'auth'

export default $axios => ({

  createSession () {
    return $axios.get('/sanctum/csrf-cookie');
  },

  login (payload) {
    return $axios.$post(`${END_POINT}/login`, payload);
  },

  logout () {
    return $axios.$post(`${END_POINT}/logout`);
  },

  register(payload) {
    return $axios.$post(`${END_POINT}/register`, payload);
  }

});
