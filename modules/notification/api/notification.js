const END_POINT = 'api/notifications'

export default $axios => ({

  get (page) {
    return $axios.$get(`${END_POINT}?page=${page}`);
  },
  view(id) {
    return $axios.$get(`${END_POINT}/${id}?view=true`);
  },
  eventGroups() {
    return $axios.$get(`${END_POINT}/event-groups`);
  }

});
