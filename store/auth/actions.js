export default {

  async singIn({ commit }, credentials) {
    try {
      await this.$repository.auth.createSession();
      const user = await this.$repository.auth.login(credentials);
      if (user) {
        commit('setUser', user)
        await this.$router.push({ name: 'dashboard' })
      }
    } catch (error) {
      console.log('singIn: ' + error);
    }
  },

  async registerUser({ commit }, payload) {
    try {
      await this.$repository.auth.createSession();
      const user = await this.$repository.auth.register(payload);
      if (user) {
        commit('setUser', user)
        await this.$router.push({ name: 'dashboard' })
      }
    } catch (error) {
      console.log('registerUser: ' + error);
    }
  },

  async singOut({ commit }) {
    try {
      await this.$repository.auth.logout();
      commit('deleteUser');
      await this.$router.push({ name: 'index' })
    } catch (error) {
      console.log('singOut' + error);
    }
  },

};
