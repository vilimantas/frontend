export default () => ({
  user: window.localStorage.user ? JSON.parse(window.localStorage.getItem('user')) : null,
})
