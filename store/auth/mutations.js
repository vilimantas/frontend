export default {
  setUser (state, user) {
    state.user = user
    window.localStorage.user = JSON.stringify(user)
  },
  deleteUser (state) {
    state.user = null
    window.localStorage.removeItem('user');
  }
}
