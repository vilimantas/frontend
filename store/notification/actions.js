export default {

  async getNotifications({ commit }, page = 1) {
    try {
      commit('notificationsIsLoading')
      let response = await this.$repository.notification.get(page);
      commit('loadNotifications', response)
      commit('notificationsIsLoading', false)
    } catch (error) {
      console.log('getNotifications' + error);
    }
  },

  async getNotification({ commit }, id) {
    try {
      commit('notificationsIsLoading')
      let response = await this.$repository.notification.view(id);
      commit('loadNotification', response)
      commit('notificationsIsLoading', false)
    } catch (error) {
      console.log('getNotification' + error);
    }
  },

};
