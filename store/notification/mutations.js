export default {

  loadNotifications (state, { data, meta }) {
    state.notifications.push(...data)
    state.meta = meta
  },

  loadNotification(state, notification ) {
    state.notification = notification
  },

  notificationsIsLoading(state, bool = true) {
    state.isLoading = bool
  },

  //for demo only

  updateNotificationStatus(state, {id, status}) {
    if (status) {
      state.meta.new = state.meta.new - 1
    } else {
      state.meta.new = state.meta.new + 1
    }
    state.notifications.find(notification => notification.id === id).is_read = status
  },
  removeNotificationById(state, id) {
    const index = state.notifications.findIndex(notification => notification.id === id)
    state.notifications.splice(index, 1)
    state.meta.new = state.meta.new - 1
  },
  markAsReadAll (state) {
    state.meta.new = 0
    state.notifications.filter(notification => !notification.is_read)
      .forEach(function (item, index, arr) {
        arr[index].is_read = true
      })
  },

}
