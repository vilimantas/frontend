export default () => ({

  notification: null,
  notifications: [],

  meta: {
    total: 0,
    new: 0,
    current_page: 0,
    last_page: 0
  },

  isLoading: false,

  notificationChannels: [
    {
      id: 1,
      name: 'Text Message'
    },
    {
      id: 2,
      name: 'Browser Notifications'
    },
    {
      id: 3,
      name: 'Email'
    },
    {
      id: 4,
      name: 'Web Page'
    },
  ],

  eventGroups: [
    {
      id: 1,
      name: 'Message Center',
      eventTypes : [
        {
          id: 1,
          name: 'Message Received'
        },
        {
          id: 2,
          name: 'Support Message Received'
        }
      ]
    },
    {
      id: 2,
      name: 'Compliance',
      eventTypes : [
        {
          id: 1,
          name: 'Product Approved'
        },
        {
          id: 2,
          name: 'Product Rejected'
        }
      ]
    },
    {
      id: 3,
      name: 'Payment Processing',
      eventTypes : [
        {
          id: 1,
          name: 'Payment Account Rejected'
        },
        {
          id: 2,
          name: 'Stripe Account Access Expired'
        }
      ]
    },
    {
      id: 4,
      name: 'Transaction Events',
      eventTypes : [
        {
          id: 1,
          name: 'Sale Notification'
        },
        {
          id: 2,
          name: 'Refund Request'
        },
        {
          id: 3,
          name: 'Dispute Opened / Closed / Lost'
        }
      ]
    },
    {
      id: 5,
      name: 'Customer Relationship',
      eventTypes : [
        {
          id: 1,
          name: 'Customer Support Request'
        },
        {
          id: 2,
          name: 'Product Downloaded'
        },
        {
          id: 3,
          name: 'Subscription Canceled'
        }
      ]
    },
    {
      id: 6,
      name: 'Account Status',
      eventTypes : [
        {
          id: 1,
          name: 'Invoice Created'
        },
        {
          id: 2,
          name: 'Invoice Past Due'
        },
        {
          id: 3,
          name: 'Customer Complaint Created'
        },
        {
          id: 4,
          name: 'Affiliate Complaint Created'
        }
      ]
    },
    {
      id: 7,
      name: 'Affiliate',
      eventTypes : [
        {
          id: 1,
          name: 'Product Promotion Opportunity'
        },
        {
          id: 2,
          name: 'Affiliate Agreement Approved'
        },
        {
          id: 3,
          name: 'Affiliate Banned'
        },
        {
          id: 4,
          name: 'JV Agreement Created'
        }
      ]
    },
  ]
})
