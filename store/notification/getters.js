export default {
  notification (state) {
    return state.notification
  },
  notifications (state) {
    return state.notifications
  },
  meta (state) {
    return state.meta
  },
  notificationsCount(state) {
    return state.notifications.length
  },
  isNotificationsLoading(state) {
    return state.isLoading
  },
  getNotificationById: (state) => id => {
    return state.notifications.find(notification => notification.id === id)
  },
  newNotifications(state) {
    return state.notifications.filter(notification => !notification.is_read)
  },
  newNotificationsCount(state) {
    return state.meta.new
  },

  notificationsIndicatorCount(state) {
    return state.meta.new > 9 ? '9+' : state.meta.new
  },

  readNotifications(state) {
    return state.notifications.filter(notification => notification.is_read)
  },
  readNotificationsCount(state) {
    return state.notifications.filter(notification => notification.is_read).length
  },
  eventGroups (state) {
    return state.eventGroups
  },
  getEventGroupById: (state) => id => {
    return state.eventGroups.find(eventGroup => eventGroup.id === id)
  },
  getNotificationChannels(state) {
    return state.notificationChannels
  }
}
