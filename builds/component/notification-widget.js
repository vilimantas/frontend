/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "5a74");
/******/ })
/************************************************************************/
/******/ ({

/***/ "0dc8":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/check_icon.d64a2141.svg";

/***/ }),

/***/ "10a6":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/arrow_bottom_black.933129f7.svg";

/***/ }),

/***/ "130b":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/settings_menu_icon.59767d36.svg";

/***/ }),

/***/ "15f7":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("baf0");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to Shadow Root
var add = __webpack_require__("35d6").default
module.exports.__inject__ = function (shadowRoot) {
  add("61e3a24c", content, shadowRoot)
};

/***/ }),

/***/ "24fb":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "2919":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/bell_circle_white.15509dca.svg";

/***/ }),

/***/ "2f62":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* unused harmony export Store */
/* unused harmony export createLogger */
/* unused harmony export createNamespacedHelpers */
/* unused harmony export install */
/* unused harmony export mapActions */
/* unused harmony export mapGetters */
/* unused harmony export mapMutations */
/* unused harmony export mapState */
/*!
 * vuex v3.6.2
 * (c) 2021 Evan You
 * @license MIT
 */
function applyMixin (Vue) {
  var version = Number(Vue.version.split('.')[0]);

  if (version >= 2) {
    Vue.mixin({ beforeCreate: vuexInit });
  } else {
    // override init and inject vuex init procedure
    // for 1.x backwards compatibility.
    var _init = Vue.prototype._init;
    Vue.prototype._init = function (options) {
      if ( options === void 0 ) options = {};

      options.init = options.init
        ? [vuexInit].concat(options.init)
        : vuexInit;
      _init.call(this, options);
    };
  }

  /**
   * Vuex init hook, injected into each instances init hooks list.
   */

  function vuexInit () {
    var options = this.$options;
    // store injection
    if (options.store) {
      this.$store = typeof options.store === 'function'
        ? options.store()
        : options.store;
    } else if (options.parent && options.parent.$store) {
      this.$store = options.parent.$store;
    }
  }
}

var target = typeof window !== 'undefined'
  ? window
  : typeof global !== 'undefined'
    ? global
    : {};
var devtoolHook = target.__VUE_DEVTOOLS_GLOBAL_HOOK__;

function devtoolPlugin (store) {
  if (!devtoolHook) { return }

  store._devtoolHook = devtoolHook;

  devtoolHook.emit('vuex:init', store);

  devtoolHook.on('vuex:travel-to-state', function (targetState) {
    store.replaceState(targetState);
  });

  store.subscribe(function (mutation, state) {
    devtoolHook.emit('vuex:mutation', mutation, state);
  }, { prepend: true });

  store.subscribeAction(function (action, state) {
    devtoolHook.emit('vuex:action', action, state);
  }, { prepend: true });
}

/**
 * Get the first item that pass the test
 * by second argument function
 *
 * @param {Array} list
 * @param {Function} f
 * @return {*}
 */
function find (list, f) {
  return list.filter(f)[0]
}

/**
 * Deep copy the given object considering circular structure.
 * This function caches all nested objects and its copies.
 * If it detects circular structure, use cached copy to avoid infinite loop.
 *
 * @param {*} obj
 * @param {Array<Object>} cache
 * @return {*}
 */
function deepCopy (obj, cache) {
  if ( cache === void 0 ) cache = [];

  // just return if obj is immutable value
  if (obj === null || typeof obj !== 'object') {
    return obj
  }

  // if obj is hit, it is in circular structure
  var hit = find(cache, function (c) { return c.original === obj; });
  if (hit) {
    return hit.copy
  }

  var copy = Array.isArray(obj) ? [] : {};
  // put the copy into cache at first
  // because we want to refer it in recursive deepCopy
  cache.push({
    original: obj,
    copy: copy
  });

  Object.keys(obj).forEach(function (key) {
    copy[key] = deepCopy(obj[key], cache);
  });

  return copy
}

/**
 * forEach for object
 */
function forEachValue (obj, fn) {
  Object.keys(obj).forEach(function (key) { return fn(obj[key], key); });
}

function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

function isPromise (val) {
  return val && typeof val.then === 'function'
}

function assert (condition, msg) {
  if (!condition) { throw new Error(("[vuex] " + msg)) }
}

function partial (fn, arg) {
  return function () {
    return fn(arg)
  }
}

// Base data struct for store's module, package with some attribute and method
var Module = function Module (rawModule, runtime) {
  this.runtime = runtime;
  // Store some children item
  this._children = Object.create(null);
  // Store the origin module object which passed by programmer
  this._rawModule = rawModule;
  var rawState = rawModule.state;

  // Store the origin module's state
  this.state = (typeof rawState === 'function' ? rawState() : rawState) || {};
};

var prototypeAccessors = { namespaced: { configurable: true } };

prototypeAccessors.namespaced.get = function () {
  return !!this._rawModule.namespaced
};

Module.prototype.addChild = function addChild (key, module) {
  this._children[key] = module;
};

Module.prototype.removeChild = function removeChild (key) {
  delete this._children[key];
};

Module.prototype.getChild = function getChild (key) {
  return this._children[key]
};

Module.prototype.hasChild = function hasChild (key) {
  return key in this._children
};

Module.prototype.update = function update (rawModule) {
  this._rawModule.namespaced = rawModule.namespaced;
  if (rawModule.actions) {
    this._rawModule.actions = rawModule.actions;
  }
  if (rawModule.mutations) {
    this._rawModule.mutations = rawModule.mutations;
  }
  if (rawModule.getters) {
    this._rawModule.getters = rawModule.getters;
  }
};

Module.prototype.forEachChild = function forEachChild (fn) {
  forEachValue(this._children, fn);
};

Module.prototype.forEachGetter = function forEachGetter (fn) {
  if (this._rawModule.getters) {
    forEachValue(this._rawModule.getters, fn);
  }
};

Module.prototype.forEachAction = function forEachAction (fn) {
  if (this._rawModule.actions) {
    forEachValue(this._rawModule.actions, fn);
  }
};

Module.prototype.forEachMutation = function forEachMutation (fn) {
  if (this._rawModule.mutations) {
    forEachValue(this._rawModule.mutations, fn);
  }
};

Object.defineProperties( Module.prototype, prototypeAccessors );

var ModuleCollection = function ModuleCollection (rawRootModule) {
  // register root module (Vuex.Store options)
  this.register([], rawRootModule, false);
};

ModuleCollection.prototype.get = function get (path) {
  return path.reduce(function (module, key) {
    return module.getChild(key)
  }, this.root)
};

ModuleCollection.prototype.getNamespace = function getNamespace (path) {
  var module = this.root;
  return path.reduce(function (namespace, key) {
    module = module.getChild(key);
    return namespace + (module.namespaced ? key + '/' : '')
  }, '')
};

ModuleCollection.prototype.update = function update$1 (rawRootModule) {
  update([], this.root, rawRootModule);
};

ModuleCollection.prototype.register = function register (path, rawModule, runtime) {
    var this$1 = this;
    if ( runtime === void 0 ) runtime = true;

  if ((false)) {}

  var newModule = new Module(rawModule, runtime);
  if (path.length === 0) {
    this.root = newModule;
  } else {
    var parent = this.get(path.slice(0, -1));
    parent.addChild(path[path.length - 1], newModule);
  }

  // register nested modules
  if (rawModule.modules) {
    forEachValue(rawModule.modules, function (rawChildModule, key) {
      this$1.register(path.concat(key), rawChildModule, runtime);
    });
  }
};

ModuleCollection.prototype.unregister = function unregister (path) {
  var parent = this.get(path.slice(0, -1));
  var key = path[path.length - 1];
  var child = parent.getChild(key);

  if (!child) {
    if ((false)) {}
    return
  }

  if (!child.runtime) {
    return
  }

  parent.removeChild(key);
};

ModuleCollection.prototype.isRegistered = function isRegistered (path) {
  var parent = this.get(path.slice(0, -1));
  var key = path[path.length - 1];

  if (parent) {
    return parent.hasChild(key)
  }

  return false
};

function update (path, targetModule, newModule) {
  if ((false)) {}

  // update target module
  targetModule.update(newModule);

  // update nested modules
  if (newModule.modules) {
    for (var key in newModule.modules) {
      if (!targetModule.getChild(key)) {
        if ((false)) {}
        return
      }
      update(
        path.concat(key),
        targetModule.getChild(key),
        newModule.modules[key]
      );
    }
  }
}

var functionAssert = {
  assert: function (value) { return typeof value === 'function'; },
  expected: 'function'
};

var objectAssert = {
  assert: function (value) { return typeof value === 'function' ||
    (typeof value === 'object' && typeof value.handler === 'function'); },
  expected: 'function or object with "handler" function'
};

var assertTypes = {
  getters: functionAssert,
  mutations: functionAssert,
  actions: objectAssert
};

function assertRawModule (path, rawModule) {
  Object.keys(assertTypes).forEach(function (key) {
    if (!rawModule[key]) { return }

    var assertOptions = assertTypes[key];

    forEachValue(rawModule[key], function (value, type) {
      assert(
        assertOptions.assert(value),
        makeAssertionMessage(path, key, type, value, assertOptions.expected)
      );
    });
  });
}

function makeAssertionMessage (path, key, type, value, expected) {
  var buf = key + " should be " + expected + " but \"" + key + "." + type + "\"";
  if (path.length > 0) {
    buf += " in module \"" + (path.join('.')) + "\"";
  }
  buf += " is " + (JSON.stringify(value)) + ".";
  return buf
}

var Vue; // bind on install

var Store = function Store (options) {
  var this$1 = this;
  if ( options === void 0 ) options = {};

  // Auto install if it is not done yet and `window` has `Vue`.
  // To allow users to avoid auto-installation in some cases,
  // this code should be placed here. See #731
  if (!Vue && typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
  }

  if ((false)) {}

  var plugins = options.plugins; if ( plugins === void 0 ) plugins = [];
  var strict = options.strict; if ( strict === void 0 ) strict = false;

  // store internal state
  this._committing = false;
  this._actions = Object.create(null);
  this._actionSubscribers = [];
  this._mutations = Object.create(null);
  this._wrappedGetters = Object.create(null);
  this._modules = new ModuleCollection(options);
  this._modulesNamespaceMap = Object.create(null);
  this._subscribers = [];
  this._watcherVM = new Vue();
  this._makeLocalGettersCache = Object.create(null);

  // bind commit and dispatch to self
  var store = this;
  var ref = this;
  var dispatch = ref.dispatch;
  var commit = ref.commit;
  this.dispatch = function boundDispatch (type, payload) {
    return dispatch.call(store, type, payload)
  };
  this.commit = function boundCommit (type, payload, options) {
    return commit.call(store, type, payload, options)
  };

  // strict mode
  this.strict = strict;

  var state = this._modules.root.state;

  // init root module.
  // this also recursively registers all sub-modules
  // and collects all module getters inside this._wrappedGetters
  installModule(this, state, [], this._modules.root);

  // initialize the store vm, which is responsible for the reactivity
  // (also registers _wrappedGetters as computed properties)
  resetStoreVM(this, state);

  // apply plugins
  plugins.forEach(function (plugin) { return plugin(this$1); });

  var useDevtools = options.devtools !== undefined ? options.devtools : Vue.config.devtools;
  if (useDevtools) {
    devtoolPlugin(this);
  }
};

var prototypeAccessors$1 = { state: { configurable: true } };

prototypeAccessors$1.state.get = function () {
  return this._vm._data.$$state
};

prototypeAccessors$1.state.set = function (v) {
  if ((false)) {}
};

Store.prototype.commit = function commit (_type, _payload, _options) {
    var this$1 = this;

  // check object-style commit
  var ref = unifyObjectStyle(_type, _payload, _options);
    var type = ref.type;
    var payload = ref.payload;
    var options = ref.options;

  var mutation = { type: type, payload: payload };
  var entry = this._mutations[type];
  if (!entry) {
    if ((false)) {}
    return
  }
  this._withCommit(function () {
    entry.forEach(function commitIterator (handler) {
      handler(payload);
    });
  });

  this._subscribers
    .slice() // shallow copy to prevent iterator invalidation if subscriber synchronously calls unsubscribe
    .forEach(function (sub) { return sub(mutation, this$1.state); });

  if (
    false
  ) {}
};

Store.prototype.dispatch = function dispatch (_type, _payload) {
    var this$1 = this;

  // check object-style dispatch
  var ref = unifyObjectStyle(_type, _payload);
    var type = ref.type;
    var payload = ref.payload;

  var action = { type: type, payload: payload };
  var entry = this._actions[type];
  if (!entry) {
    if ((false)) {}
    return
  }

  try {
    this._actionSubscribers
      .slice() // shallow copy to prevent iterator invalidation if subscriber synchronously calls unsubscribe
      .filter(function (sub) { return sub.before; })
      .forEach(function (sub) { return sub.before(action, this$1.state); });
  } catch (e) {
    if ((false)) {}
  }

  var result = entry.length > 1
    ? Promise.all(entry.map(function (handler) { return handler(payload); }))
    : entry[0](payload);

  return new Promise(function (resolve, reject) {
    result.then(function (res) {
      try {
        this$1._actionSubscribers
          .filter(function (sub) { return sub.after; })
          .forEach(function (sub) { return sub.after(action, this$1.state); });
      } catch (e) {
        if ((false)) {}
      }
      resolve(res);
    }, function (error) {
      try {
        this$1._actionSubscribers
          .filter(function (sub) { return sub.error; })
          .forEach(function (sub) { return sub.error(action, this$1.state, error); });
      } catch (e) {
        if ((false)) {}
      }
      reject(error);
    });
  })
};

Store.prototype.subscribe = function subscribe (fn, options) {
  return genericSubscribe(fn, this._subscribers, options)
};

Store.prototype.subscribeAction = function subscribeAction (fn, options) {
  var subs = typeof fn === 'function' ? { before: fn } : fn;
  return genericSubscribe(subs, this._actionSubscribers, options)
};

Store.prototype.watch = function watch (getter, cb, options) {
    var this$1 = this;

  if ((false)) {}
  return this._watcherVM.$watch(function () { return getter(this$1.state, this$1.getters); }, cb, options)
};

Store.prototype.replaceState = function replaceState (state) {
    var this$1 = this;

  this._withCommit(function () {
    this$1._vm._data.$$state = state;
  });
};

Store.prototype.registerModule = function registerModule (path, rawModule, options) {
    if ( options === void 0 ) options = {};

  if (typeof path === 'string') { path = [path]; }

  if ((false)) {}

  this._modules.register(path, rawModule);
  installModule(this, this.state, path, this._modules.get(path), options.preserveState);
  // reset store to update getters...
  resetStoreVM(this, this.state);
};

Store.prototype.unregisterModule = function unregisterModule (path) {
    var this$1 = this;

  if (typeof path === 'string') { path = [path]; }

  if ((false)) {}

  this._modules.unregister(path);
  this._withCommit(function () {
    var parentState = getNestedState(this$1.state, path.slice(0, -1));
    Vue.delete(parentState, path[path.length - 1]);
  });
  resetStore(this);
};

Store.prototype.hasModule = function hasModule (path) {
  if (typeof path === 'string') { path = [path]; }

  if ((false)) {}

  return this._modules.isRegistered(path)
};

Store.prototype.hotUpdate = function hotUpdate (newOptions) {
  this._modules.update(newOptions);
  resetStore(this, true);
};

Store.prototype._withCommit = function _withCommit (fn) {
  var committing = this._committing;
  this._committing = true;
  fn();
  this._committing = committing;
};

Object.defineProperties( Store.prototype, prototypeAccessors$1 );

function genericSubscribe (fn, subs, options) {
  if (subs.indexOf(fn) < 0) {
    options && options.prepend
      ? subs.unshift(fn)
      : subs.push(fn);
  }
  return function () {
    var i = subs.indexOf(fn);
    if (i > -1) {
      subs.splice(i, 1);
    }
  }
}

function resetStore (store, hot) {
  store._actions = Object.create(null);
  store._mutations = Object.create(null);
  store._wrappedGetters = Object.create(null);
  store._modulesNamespaceMap = Object.create(null);
  var state = store.state;
  // init all modules
  installModule(store, state, [], store._modules.root, true);
  // reset vm
  resetStoreVM(store, state, hot);
}

function resetStoreVM (store, state, hot) {
  var oldVm = store._vm;

  // bind store public getters
  store.getters = {};
  // reset local getters cache
  store._makeLocalGettersCache = Object.create(null);
  var wrappedGetters = store._wrappedGetters;
  var computed = {};
  forEachValue(wrappedGetters, function (fn, key) {
    // use computed to leverage its lazy-caching mechanism
    // direct inline function use will lead to closure preserving oldVm.
    // using partial to return function with only arguments preserved in closure environment.
    computed[key] = partial(fn, store);
    Object.defineProperty(store.getters, key, {
      get: function () { return store._vm[key]; },
      enumerable: true // for local getters
    });
  });

  // use a Vue instance to store the state tree
  // suppress warnings just in case the user has added
  // some funky global mixins
  var silent = Vue.config.silent;
  Vue.config.silent = true;
  store._vm = new Vue({
    data: {
      $$state: state
    },
    computed: computed
  });
  Vue.config.silent = silent;

  // enable strict mode for new vm
  if (store.strict) {
    enableStrictMode(store);
  }

  if (oldVm) {
    if (hot) {
      // dispatch changes in all subscribed watchers
      // to force getter re-evaluation for hot reloading.
      store._withCommit(function () {
        oldVm._data.$$state = null;
      });
    }
    Vue.nextTick(function () { return oldVm.$destroy(); });
  }
}

function installModule (store, rootState, path, module, hot) {
  var isRoot = !path.length;
  var namespace = store._modules.getNamespace(path);

  // register in namespace map
  if (module.namespaced) {
    if (store._modulesNamespaceMap[namespace] && ("production" !== 'production')) {
      console.error(("[vuex] duplicate namespace " + namespace + " for the namespaced module " + (path.join('/'))));
    }
    store._modulesNamespaceMap[namespace] = module;
  }

  // set state
  if (!isRoot && !hot) {
    var parentState = getNestedState(rootState, path.slice(0, -1));
    var moduleName = path[path.length - 1];
    store._withCommit(function () {
      if ((false)) {}
      Vue.set(parentState, moduleName, module.state);
    });
  }

  var local = module.context = makeLocalContext(store, namespace, path);

  module.forEachMutation(function (mutation, key) {
    var namespacedType = namespace + key;
    registerMutation(store, namespacedType, mutation, local);
  });

  module.forEachAction(function (action, key) {
    var type = action.root ? key : namespace + key;
    var handler = action.handler || action;
    registerAction(store, type, handler, local);
  });

  module.forEachGetter(function (getter, key) {
    var namespacedType = namespace + key;
    registerGetter(store, namespacedType, getter, local);
  });

  module.forEachChild(function (child, key) {
    installModule(store, rootState, path.concat(key), child, hot);
  });
}

/**
 * make localized dispatch, commit, getters and state
 * if there is no namespace, just use root ones
 */
function makeLocalContext (store, namespace, path) {
  var noNamespace = namespace === '';

  var local = {
    dispatch: noNamespace ? store.dispatch : function (_type, _payload, _options) {
      var args = unifyObjectStyle(_type, _payload, _options);
      var payload = args.payload;
      var options = args.options;
      var type = args.type;

      if (!options || !options.root) {
        type = namespace + type;
        if (false) {}
      }

      return store.dispatch(type, payload)
    },

    commit: noNamespace ? store.commit : function (_type, _payload, _options) {
      var args = unifyObjectStyle(_type, _payload, _options);
      var payload = args.payload;
      var options = args.options;
      var type = args.type;

      if (!options || !options.root) {
        type = namespace + type;
        if (false) {}
      }

      store.commit(type, payload, options);
    }
  };

  // getters and state object must be gotten lazily
  // because they will be changed by vm update
  Object.defineProperties(local, {
    getters: {
      get: noNamespace
        ? function () { return store.getters; }
        : function () { return makeLocalGetters(store, namespace); }
    },
    state: {
      get: function () { return getNestedState(store.state, path); }
    }
  });

  return local
}

function makeLocalGetters (store, namespace) {
  if (!store._makeLocalGettersCache[namespace]) {
    var gettersProxy = {};
    var splitPos = namespace.length;
    Object.keys(store.getters).forEach(function (type) {
      // skip if the target getter is not match this namespace
      if (type.slice(0, splitPos) !== namespace) { return }

      // extract local getter type
      var localType = type.slice(splitPos);

      // Add a port to the getters proxy.
      // Define as getter property because
      // we do not want to evaluate the getters in this time.
      Object.defineProperty(gettersProxy, localType, {
        get: function () { return store.getters[type]; },
        enumerable: true
      });
    });
    store._makeLocalGettersCache[namespace] = gettersProxy;
  }

  return store._makeLocalGettersCache[namespace]
}

function registerMutation (store, type, handler, local) {
  var entry = store._mutations[type] || (store._mutations[type] = []);
  entry.push(function wrappedMutationHandler (payload) {
    handler.call(store, local.state, payload);
  });
}

function registerAction (store, type, handler, local) {
  var entry = store._actions[type] || (store._actions[type] = []);
  entry.push(function wrappedActionHandler (payload) {
    var res = handler.call(store, {
      dispatch: local.dispatch,
      commit: local.commit,
      getters: local.getters,
      state: local.state,
      rootGetters: store.getters,
      rootState: store.state
    }, payload);
    if (!isPromise(res)) {
      res = Promise.resolve(res);
    }
    if (store._devtoolHook) {
      return res.catch(function (err) {
        store._devtoolHook.emit('vuex:error', err);
        throw err
      })
    } else {
      return res
    }
  });
}

function registerGetter (store, type, rawGetter, local) {
  if (store._wrappedGetters[type]) {
    if ((false)) {}
    return
  }
  store._wrappedGetters[type] = function wrappedGetter (store) {
    return rawGetter(
      local.state, // local state
      local.getters, // local getters
      store.state, // root state
      store.getters // root getters
    )
  };
}

function enableStrictMode (store) {
  store._vm.$watch(function () { return this._data.$$state }, function () {
    if ((false)) {}
  }, { deep: true, sync: true });
}

function getNestedState (state, path) {
  return path.reduce(function (state, key) { return state[key]; }, state)
}

function unifyObjectStyle (type, payload, options) {
  if (isObject(type) && type.type) {
    options = payload;
    payload = type;
    type = type.type;
  }

  if ((false)) {}

  return { type: type, payload: payload, options: options }
}

function install (_Vue) {
  if (Vue && _Vue === Vue) {
    if ((false)) {}
    return
  }
  Vue = _Vue;
  applyMixin(Vue);
}

/**
 * Reduce the code which written in Vue.js for getting the state.
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} states # Object's item can be a function which accept state and getters for param, you can do something for state and getters in it.
 * @param {Object}
 */
var mapState = normalizeNamespace(function (namespace, states) {
  var res = {};
  if (false) {}
  normalizeMap(states).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedState () {
      var state = this.$store.state;
      var getters = this.$store.getters;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapState', namespace);
        if (!module) {
          return
        }
        state = module.context.state;
        getters = module.context.getters;
      }
      return typeof val === 'function'
        ? val.call(this, state, getters)
        : state[val]
    };
    // mark vuex getter for devtools
    res[key].vuex = true;
  });
  return res
});

/**
 * Reduce the code which written in Vue.js for committing the mutation
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} mutations # Object's item can be a function which accept `commit` function as the first param, it can accept another params. You can commit mutation and do any other things in this function. specially, You need to pass anthor params from the mapped function.
 * @return {Object}
 */
var mapMutations = normalizeNamespace(function (namespace, mutations) {
  var res = {};
  if (false) {}
  normalizeMap(mutations).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedMutation () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      // Get the commit method from store
      var commit = this.$store.commit;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapMutations', namespace);
        if (!module) {
          return
        }
        commit = module.context.commit;
      }
      return typeof val === 'function'
        ? val.apply(this, [commit].concat(args))
        : commit.apply(this.$store, [val].concat(args))
    };
  });
  return res
});

/**
 * Reduce the code which written in Vue.js for getting the getters
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} getters
 * @return {Object}
 */
var mapGetters = normalizeNamespace(function (namespace, getters) {
  var res = {};
  if (false) {}
  normalizeMap(getters).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    // The namespace has been mutated by normalizeNamespace
    val = namespace + val;
    res[key] = function mappedGetter () {
      if (namespace && !getModuleByNamespace(this.$store, 'mapGetters', namespace)) {
        return
      }
      if (false) {}
      return this.$store.getters[val]
    };
    // mark vuex getter for devtools
    res[key].vuex = true;
  });
  return res
});

/**
 * Reduce the code which written in Vue.js for dispatch the action
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} actions # Object's item can be a function which accept `dispatch` function as the first param, it can accept anthor params. You can dispatch action and do any other things in this function. specially, You need to pass anthor params from the mapped function.
 * @return {Object}
 */
var mapActions = normalizeNamespace(function (namespace, actions) {
  var res = {};
  if (false) {}
  normalizeMap(actions).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedAction () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      // get dispatch function from store
      var dispatch = this.$store.dispatch;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapActions', namespace);
        if (!module) {
          return
        }
        dispatch = module.context.dispatch;
      }
      return typeof val === 'function'
        ? val.apply(this, [dispatch].concat(args))
        : dispatch.apply(this.$store, [val].concat(args))
    };
  });
  return res
});

/**
 * Rebinding namespace param for mapXXX function in special scoped, and return them by simple object
 * @param {String} namespace
 * @return {Object}
 */
var createNamespacedHelpers = function (namespace) { return ({
  mapState: mapState.bind(null, namespace),
  mapGetters: mapGetters.bind(null, namespace),
  mapMutations: mapMutations.bind(null, namespace),
  mapActions: mapActions.bind(null, namespace)
}); };

/**
 * Normalize the map
 * normalizeMap([1, 2, 3]) => [ { key: 1, val: 1 }, { key: 2, val: 2 }, { key: 3, val: 3 } ]
 * normalizeMap({a: 1, b: 2, c: 3}) => [ { key: 'a', val: 1 }, { key: 'b', val: 2 }, { key: 'c', val: 3 } ]
 * @param {Array|Object} map
 * @return {Object}
 */
function normalizeMap (map) {
  if (!isValidMap(map)) {
    return []
  }
  return Array.isArray(map)
    ? map.map(function (key) { return ({ key: key, val: key }); })
    : Object.keys(map).map(function (key) { return ({ key: key, val: map[key] }); })
}

/**
 * Validate whether given map is valid or not
 * @param {*} map
 * @return {Boolean}
 */
function isValidMap (map) {
  return Array.isArray(map) || isObject(map)
}

/**
 * Return a function expect two param contains namespace and map. it will normalize the namespace and then the param's function will handle the new namespace and the map.
 * @param {Function} fn
 * @return {Function}
 */
function normalizeNamespace (fn) {
  return function (namespace, map) {
    if (typeof namespace !== 'string') {
      map = namespace;
      namespace = '';
    } else if (namespace.charAt(namespace.length - 1) !== '/') {
      namespace += '/';
    }
    return fn(namespace, map)
  }
}

/**
 * Search a special module from store by namespace. if module not exist, print error message.
 * @param {Object} store
 * @param {String} helper
 * @param {String} namespace
 * @return {Object}
 */
function getModuleByNamespace (store, helper, namespace) {
  var module = store._modulesNamespaceMap[namespace];
  if (false) {}
  return module
}

// Credits: borrowed code from fcomb/redux-logger

function createLogger (ref) {
  if ( ref === void 0 ) ref = {};
  var collapsed = ref.collapsed; if ( collapsed === void 0 ) collapsed = true;
  var filter = ref.filter; if ( filter === void 0 ) filter = function (mutation, stateBefore, stateAfter) { return true; };
  var transformer = ref.transformer; if ( transformer === void 0 ) transformer = function (state) { return state; };
  var mutationTransformer = ref.mutationTransformer; if ( mutationTransformer === void 0 ) mutationTransformer = function (mut) { return mut; };
  var actionFilter = ref.actionFilter; if ( actionFilter === void 0 ) actionFilter = function (action, state) { return true; };
  var actionTransformer = ref.actionTransformer; if ( actionTransformer === void 0 ) actionTransformer = function (act) { return act; };
  var logMutations = ref.logMutations; if ( logMutations === void 0 ) logMutations = true;
  var logActions = ref.logActions; if ( logActions === void 0 ) logActions = true;
  var logger = ref.logger; if ( logger === void 0 ) logger = console;

  return function (store) {
    var prevState = deepCopy(store.state);

    if (typeof logger === 'undefined') {
      return
    }

    if (logMutations) {
      store.subscribe(function (mutation, state) {
        var nextState = deepCopy(state);

        if (filter(mutation, prevState, nextState)) {
          var formattedTime = getFormattedTime();
          var formattedMutation = mutationTransformer(mutation);
          var message = "mutation " + (mutation.type) + formattedTime;

          startMessage(logger, message, collapsed);
          logger.log('%c prev state', 'color: #9E9E9E; font-weight: bold', transformer(prevState));
          logger.log('%c mutation', 'color: #03A9F4; font-weight: bold', formattedMutation);
          logger.log('%c next state', 'color: #4CAF50; font-weight: bold', transformer(nextState));
          endMessage(logger);
        }

        prevState = nextState;
      });
    }

    if (logActions) {
      store.subscribeAction(function (action, state) {
        if (actionFilter(action, state)) {
          var formattedTime = getFormattedTime();
          var formattedAction = actionTransformer(action);
          var message = "action " + (action.type) + formattedTime;

          startMessage(logger, message, collapsed);
          logger.log('%c action', 'color: #03A9F4; font-weight: bold', formattedAction);
          endMessage(logger);
        }
      });
    }
  }
}

function startMessage (logger, message, collapsed) {
  var startMessage = collapsed
    ? logger.groupCollapsed
    : logger.group;

  // render
  try {
    startMessage.call(logger, message);
  } catch (e) {
    logger.log(message);
  }
}

function endMessage (logger) {
  try {
    logger.groupEnd();
  } catch (e) {
    logger.log('—— log end ——');
  }
}

function getFormattedTime () {
  var time = new Date();
  return (" @ " + (pad(time.getHours(), 2)) + ":" + (pad(time.getMinutes(), 2)) + ":" + (pad(time.getSeconds(), 2)) + "." + (pad(time.getMilliseconds(), 3)))
}

function repeat (str, times) {
  return (new Array(times + 1)).join(str)
}

function pad (num, maxLength) {
  return repeat('0', maxLength - num.toString().length) + num
}

var index = {
  Store: Store,
  install: install,
  version: '3.6.2',
  mapState: mapState,
  mapMutations: mapMutations,
  mapGetters: mapGetters,
  mapActions: mapActions,
  createNamespacedHelpers: createNamespacedHelpers,
  createLogger: createLogger
};

/* harmony default export */ __webpack_exports__["a"] = (index);


/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("c8ba")))

/***/ }),

/***/ "3075":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/empty_state.dea04557.svg";

/***/ }),

/***/ "35d6":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "default", function() { return /* binding */ addStylesToShadowDOM; });

// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/listToStyles.js
/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}

// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/addStylesShadow.js


function addStylesToShadowDOM (parentId, list, shadowRoot) {
  var styles = listToStyles(parentId, list)
  addStyles(styles, shadowRoot)
}

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

function addStyles (styles /* Array<StyleObject> */, shadowRoot) {
  const injectedStyles =
    shadowRoot._injectedStyles ||
    (shadowRoot._injectedStyles = {})
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var style = injectedStyles[item.id]
    if (!style) {
      for (var j = 0; j < item.parts.length; j++) {
        addStyle(item.parts[j], shadowRoot)
      }
      injectedStyles[item.id] = true
    }
  }
}

function createStyleElement (shadowRoot) {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  shadowRoot.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */, shadowRoot) {
  var styleElement = createStyleElement(shadowRoot)
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ "44b2":
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__("24fb");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "#widget[data-v-699abe6b]{font-family:Inter,sans-serif}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "497d":
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__("24fb");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "/* ! tailwindcss v2.1.0 | MIT License | https://tailwindcss.com */\n\n/*! modern-normalize v1.0.0 | MIT License | https://github.com/sindresorhus/modern-normalize */:root{-moz-tab-size:4;-o-tab-size:4;tab-size:4}html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0;font-family:system-ui,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji}hr{height:0;color:inherit}abbr[title]{-webkit-text-decoration:underline dotted;text-decoration:underline dotted}b,strong{font-weight:bolder}code,kbd,pre,samp{font-family:ui-monospace,SFMono-Regular,Consolas,Liberation Mono,Menlo,monospace;font-size:1em}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}table{text-indent:0;border-color:inherit}button,input,optgroup,select,textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}button,select{text-transform:none}[type=button],button{-webkit-appearance:button}legend{padding:0}progress{vertical-align:baseline}summary{display:list-item}blockquote,dd,dl,figure,h1,h2,h3,h4,h5,h6,hr,p,pre{margin:0}button{background-color:transparent;background-image:none}button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}fieldset,ol,ul{margin:0;padding:0}ol,ul{list-style:none}html{font-family:ui-sans-serif,system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}body{font-family:inherit;line-height:inherit}*,:after,:before{box-sizing:border-box;border-width:0;border-style:solid;border-color:#e5e7eb}hr{border-top-width:1px}img{border-style:solid}textarea{resize:vertical}input::-moz-placeholder,textarea::-moz-placeholder{opacity:1;color:#9ca3af}input:-ms-input-placeholder,textarea:-ms-input-placeholder{opacity:1;color:#9ca3af}input::placeholder,textarea::placeholder{opacity:1;color:#9ca3af}button{cursor:pointer}table{border-collapse:collapse}h1,h2,h3,h4,h5,h6{font-size:inherit;font-weight:inherit}a{color:inherit;text-decoration:inherit}button,input,optgroup,select,textarea{padding:0;line-height:inherit;color:inherit}code,kbd,pre,samp{font-family:ui-monospace,SFMono-Regular,Menlo,Monaco,Consolas,Liberation Mono,Courier New,monospace}audio,canvas,embed,iframe,img,object,svg,video{display:block;vertical-align:middle}img,video{max-width:100%;height:auto}body{--tw-bg-opacity:1;background-color:rgba(243,244,246,var(--tw-bg-opacity));font-family:ui-sans-serif,system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;height:100vh}h1{font-size:1.5rem;line-height:2rem}h2{font-size:1.25rem;line-height:1.75rem}.bg-white{--tw-bg-opacity:1;background-color:rgba(255,255,255,var(--tw-bg-opacity))}.bg-red-700{--tw-bg-opacity:1;background-color:rgba(185,28,28,var(--tw-bg-opacity))}.bg-blue-700{--tw-bg-opacity:1;background-color:rgba(29,78,216,var(--tw-bg-opacity))}.hover\\:bg-gray-200:hover{--tw-bg-opacity:1;background-color:rgba(229,231,235,var(--tw-bg-opacity))}.border-gray-300{--tw-border-opacity:1;border-color:rgba(209,213,219,var(--tw-border-opacity))}.rounded-lg{border-radius:.5rem}.rounded-full{border-radius:9999px}.rounded-t-lg{border-top-left-radius:.5rem;border-top-right-radius:.5rem}.rounded-b-lg{border-bottom-right-radius:.5rem;border-bottom-left-radius:.5rem}.border{border-width:1px}.border-b{border-bottom-width:1px}.cursor-default{cursor:default}.cursor-pointer{cursor:pointer}.block{display:block}.flex{display:flex}.inline-flex{display:inline-flex}.table{display:table}.flex-col{flex-direction:column}.items-center{align-items:center}.justify-center{justify-content:center}.justify-between{justify-content:space-between}.font-normal{font-weight:400}.font-medium{font-weight:500}.font-bold{font-weight:700}.h-2{height:.5rem}.h-5{height:1.25rem}.h-6{height:1.5rem}.h-10{height:2.5rem}.h-12{height:3rem}.h-24{height:6rem}.h-screen{height:100vh}.text-xs{font-size:.75rem;line-height:1rem}.text-sm{font-size:.875rem;line-height:1.25rem}.text-base{font-size:1rem;line-height:1.5rem}.-m-2{margin:-.5rem}.my-4{margin-top:1rem;margin-bottom:1rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.mb-2{margin-bottom:.5rem}.mt-4{margin-top:1rem}.mr-4{margin-right:1rem}.mb-4{margin-bottom:1rem}.mt-8{margin-top:2rem}.-mt-1{margin-top:-.25rem}.-mr-1{margin-right:-.25rem}.max-h-24{max-height:6rem}.max-h-96{max-height:24rem}.min-h-full{min-height:100%}.focus\\:outline-none:focus{outline:2px solid transparent;outline-offset:2px}.overflow-y-auto{overflow-y:auto}.p-2{padding:.5rem}.py-2{padding-top:.5rem;padding-bottom:.5rem}.px-2{padding-left:.5rem;padding-right:.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-4{padding-left:1rem;padding-right:1rem}.py-6{padding-top:1.5rem;padding-bottom:1.5rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pb-2{padding-bottom:.5rem}.pt-4{padding-top:1rem}.fixed{position:fixed}.absolute{position:absolute}.relative{position:relative}.top-0{top:0}.right-0{right:0}.left-0{left:0}.right-2{right:.5rem}.top-8{top:2rem}.top-20{top:5rem}*{--tw-shadow:0 0 transparent}.shadow-md{--tw-shadow:0 4px 6px -1px rgba(0,0,0,0.1),0 2px 4px -1px rgba(0,0,0,0.06);box-shadow:var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),var(--tw-shadow)}*{--tw-ring-inset:var(--tw-empty,/*!*/ /*!*/);--tw-ring-offset-width:0px;--tw-ring-offset-color:#fff;--tw-ring-color:rgba(59,130,246,0.5);--tw-ring-offset-shadow:0 0 transparent;--tw-ring-shadow:0 0 transparent}.text-black{--tw-text-opacity:1;color:rgba(0,0,0,var(--tw-text-opacity))}.text-white{--tw-text-opacity:1;color:rgba(255,255,255,var(--tw-text-opacity))}.text-gray-500{--tw-text-opacity:1;color:rgba(107,114,128,var(--tw-text-opacity))}.text-blue-700{--tw-text-opacity:1;color:rgba(29,78,216,var(--tw-text-opacity))}.w-2{width:.5rem}.w-5{width:1.25rem}.w-6{width:1.5rem}.w-10{width:2.5rem}.w-12{width:3rem}.w-full{width:100%}.w-screen{width:100vw}.z-40{z-index:40}.z-50{z-index:50}.transform{--tw-translate-x:0;--tw-translate-y:0;--tw-rotate:0;--tw-skew-x:0;--tw-skew-y:0;--tw-scale-x:1;--tw-scale-y:1;transform:translateX(var(--tw-translate-x)) translateY(var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y))}.transition{transition-property:background-color,border-color,color,fill,stroke,opacity,box-shadow,transform,filter,-webkit-backdrop-filter;transition-property:background-color,border-color,color,fill,stroke,opacity,box-shadow,transform,filter,backdrop-filter;transition-property:background-color,border-color,color,fill,stroke,opacity,box-shadow,transform,filter,backdrop-filter,-webkit-backdrop-filter;transition-timing-function:cubic-bezier(.4,0,.2,1);transition-duration:.15s}@-webkit-keyframes spin{to{transform:rotate(1turn)}}@keyframes spin{to{transform:rotate(1turn)}}@-webkit-keyframes ping{75%,to{transform:scale(2);opacity:0}}@keyframes ping{75%,to{transform:scale(2);opacity:0}}@-webkit-keyframes pulse{50%{opacity:.5}}@keyframes pulse{50%{opacity:.5}}@-webkit-keyframes bounce{0%,to{transform:translateY(-25%);-webkit-animation-timing-function:cubic-bezier(.8,0,1,1);animation-timing-function:cubic-bezier(.8,0,1,1)}50%{transform:none;-webkit-animation-timing-function:cubic-bezier(0,0,.2,1);animation-timing-function:cubic-bezier(0,0,.2,1)}}@keyframes bounce{0%,to{transform:translateY(-25%);-webkit-animation-timing-function:cubic-bezier(.8,0,1,1);animation-timing-function:cubic-bezier(.8,0,1,1)}50%{transform:none;-webkit-animation-timing-function:cubic-bezier(0,0,.2,1);animation-timing-function:cubic-bezier(0,0,.2,1)}}.filter{--tw-blur:var(--tw-empty,/*!*/ /*!*/);--tw-brightness:var(--tw-empty,/*!*/ /*!*/);--tw-contrast:var(--tw-empty,/*!*/ /*!*/);--tw-grayscale:var(--tw-empty,/*!*/ /*!*/);--tw-hue-rotate:var(--tw-empty,/*!*/ /*!*/);--tw-invert:var(--tw-empty,/*!*/ /*!*/);--tw-saturate:var(--tw-empty,/*!*/ /*!*/);--tw-sepia:var(--tw-empty,/*!*/ /*!*/);--tw-drop-shadow:var(--tw-empty,/*!*/ /*!*/);filter:var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow)}@media (min-width:768px){.md\\:px-0{padding-left:0;padding-right:0}.md\\:right-8{right:2rem}.md\\:w-72{width:18rem}.md\\:w-80{width:20rem}}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "4ce9":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("44b2");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to Shadow Root
var add = __webpack_require__("35d6").default
module.exports.__inject__ = function (shadowRoot) {
  add("73998d10", content, shadowRoot)
};

/***/ }),

/***/ "5370":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_3bd10818_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("15f7");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_3bd10818_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_3bd10818_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_3bd10818_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_3bd10818_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "5a74":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var currentScript = window.document.currentScript
  if (Object({"NODE_ENV":"production","BASE_URL":"/"}).NEED_CURRENTSCRIPT_POLYFILL) {
    var getCurrentScript = __webpack_require__("8875")
    currentScript = getCurrentScript()

    // for backward compatibility, because previously we directly included the polyfill
    if (!('currentScript' in document)) {
      Object.defineProperty(document, 'currentScript', { get: getCurrentScript })
    }
  }

  var src = currentScript && currentScript.src.match(/(.+\/)[^/]+\.js(\?.*)?$/)
  if (src) {
    __webpack_require__.p = src[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// EXTERNAL MODULE: external "Vue"
var external_Vue_ = __webpack_require__("8bbf");
var external_Vue_default = /*#__PURE__*/__webpack_require__.n(external_Vue_);

// CONCATENATED MODULE: ./node_modules/@vue/web-component-wrapper/dist/vue-wc-wrapper.js
const camelizeRE = /-(\w)/g;
const camelize = str => {
  return str.replace(camelizeRE, (_, c) => c ? c.toUpperCase() : '')
};

const hyphenateRE = /\B([A-Z])/g;
const hyphenate = str => {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
};

function getInitialProps (propsList) {
  const res = {};
  propsList.forEach(key => {
    res[key] = undefined;
  });
  return res
}

function injectHook (options, key, hook) {
  options[key] = [].concat(options[key] || []);
  options[key].unshift(hook);
}

function callHooks (vm, hook) {
  if (vm) {
    const hooks = vm.$options[hook] || [];
    hooks.forEach(hook => {
      hook.call(vm);
    });
  }
}

function createCustomEvent (name, args) {
  return new CustomEvent(name, {
    bubbles: false,
    cancelable: false,
    detail: args
  })
}

const isBoolean = val => /function Boolean/.test(String(val));
const isNumber = val => /function Number/.test(String(val));

function convertAttributeValue (value, name, { type } = {}) {
  if (isBoolean(type)) {
    if (value === 'true' || value === 'false') {
      return value === 'true'
    }
    if (value === '' || value === name || value != null) {
      return true
    }
    return value
  } else if (isNumber(type)) {
    const parsed = parseFloat(value, 10);
    return isNaN(parsed) ? value : parsed
  } else {
    return value
  }
}

function toVNodes (h, children) {
  const res = [];
  for (let i = 0, l = children.length; i < l; i++) {
    res.push(toVNode(h, children[i]));
  }
  return res
}

function toVNode (h, node) {
  if (node.nodeType === 3) {
    return node.data.trim() ? node.data : null
  } else if (node.nodeType === 1) {
    const data = {
      attrs: getAttributes(node),
      domProps: {
        innerHTML: node.innerHTML
      }
    };
    if (data.attrs.slot) {
      data.slot = data.attrs.slot;
      delete data.attrs.slot;
    }
    return h(node.tagName, data)
  } else {
    return null
  }
}

function getAttributes (node) {
  const res = {};
  for (let i = 0, l = node.attributes.length; i < l; i++) {
    const attr = node.attributes[i];
    res[attr.nodeName] = attr.nodeValue;
  }
  return res
}

function wrap (Vue, Component) {
  const isAsync = typeof Component === 'function' && !Component.cid;
  let isInitialized = false;
  let hyphenatedPropsList;
  let camelizedPropsList;
  let camelizedPropsMap;

  function initialize (Component) {
    if (isInitialized) return

    const options = typeof Component === 'function'
      ? Component.options
      : Component;

    // extract props info
    const propsList = Array.isArray(options.props)
      ? options.props
      : Object.keys(options.props || {});
    hyphenatedPropsList = propsList.map(hyphenate);
    camelizedPropsList = propsList.map(camelize);
    const originalPropsAsObject = Array.isArray(options.props) ? {} : options.props || {};
    camelizedPropsMap = camelizedPropsList.reduce((map, key, i) => {
      map[key] = originalPropsAsObject[propsList[i]];
      return map
    }, {});

    // proxy $emit to native DOM events
    injectHook(options, 'beforeCreate', function () {
      const emit = this.$emit;
      this.$emit = (name, ...args) => {
        this.$root.$options.customElement.dispatchEvent(createCustomEvent(name, args));
        return emit.call(this, name, ...args)
      };
    });

    injectHook(options, 'created', function () {
      // sync default props values to wrapper on created
      camelizedPropsList.forEach(key => {
        this.$root.props[key] = this[key];
      });
    });

    // proxy props as Element properties
    camelizedPropsList.forEach(key => {
      Object.defineProperty(CustomElement.prototype, key, {
        get () {
          return this._wrapper.props[key]
        },
        set (newVal) {
          this._wrapper.props[key] = newVal;
        },
        enumerable: false,
        configurable: true
      });
    });

    isInitialized = true;
  }

  function syncAttribute (el, key) {
    const camelized = camelize(key);
    const value = el.hasAttribute(key) ? el.getAttribute(key) : undefined;
    el._wrapper.props[camelized] = convertAttributeValue(
      value,
      key,
      camelizedPropsMap[camelized]
    );
  }

  class CustomElement extends HTMLElement {
    constructor () {
      const self = super();
      self.attachShadow({ mode: 'open' });

      const wrapper = self._wrapper = new Vue({
        name: 'shadow-root',
        customElement: self,
        shadowRoot: self.shadowRoot,
        data () {
          return {
            props: {},
            slotChildren: []
          }
        },
        render (h) {
          return h(Component, {
            ref: 'inner',
            props: this.props
          }, this.slotChildren)
        }
      });

      // Use MutationObserver to react to future attribute & slot content change
      const observer = new MutationObserver(mutations => {
        let hasChildrenChange = false;
        for (let i = 0; i < mutations.length; i++) {
          const m = mutations[i];
          if (isInitialized && m.type === 'attributes' && m.target === self) {
            syncAttribute(self, m.attributeName);
          } else {
            hasChildrenChange = true;
          }
        }
        if (hasChildrenChange) {
          wrapper.slotChildren = Object.freeze(toVNodes(
            wrapper.$createElement,
            self.childNodes
          ));
        }
      });
      observer.observe(self, {
        childList: true,
        subtree: true,
        characterData: true,
        attributes: true
      });
    }

    get vueComponent () {
      return this._wrapper.$refs.inner
    }

    connectedCallback () {
      const wrapper = this._wrapper;
      if (!wrapper._isMounted) {
        // initialize attributes
        const syncInitialAttributes = () => {
          wrapper.props = getInitialProps(camelizedPropsList);
          hyphenatedPropsList.forEach(key => {
            syncAttribute(this, key);
          });
        };

        if (isInitialized) {
          syncInitialAttributes();
        } else {
          // async & unresolved
          Component().then(resolved => {
            if (resolved.__esModule || resolved[Symbol.toStringTag] === 'Module') {
              resolved = resolved.default;
            }
            initialize(resolved);
            syncInitialAttributes();
          });
        }
        // initialize children
        wrapper.slotChildren = Object.freeze(toVNodes(
          wrapper.$createElement,
          this.childNodes
        ));
        wrapper.$mount();
        this.shadowRoot.appendChild(wrapper.$el);
      } else {
        callHooks(this.vueComponent, 'activated');
      }
    }

    disconnectedCallback () {
      callHooks(this.vueComponent, 'deactivated');
    }
  }

  if (!isAsync) {
    initialize(Component);
  }

  return CustomElement
}

/* harmony default export */ var vue_wc_wrapper = (wrap);

// EXTERNAL MODULE: ./node_modules/css-loader/dist/runtime/api.js
var api = __webpack_require__("24fb");

// EXTERNAL MODULE: ./node_modules/vue-style-loader/lib/addStylesShadow.js + 1 modules
var addStylesShadow = __webpack_require__("35d6");

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"648b734d-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/NotificationModule.vue?vue&type=template&id=699abe6b&scoped=true&shadow
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.moduleIsReady)?_c('div',{attrs:{"id":"widget"}},[_c('floating-button',{attrs:{"show-notifications-window":_vm.showNotificationsWindow},on:{"update:showNotificationsWindow":function($event){_vm.showNotificationsWindow=$event},"update:show-notifications-window":function($event){_vm.showNotificationsWindow=$event}}}),(_vm.showNotificationsWindow)?_c('notification-window',{scopedSlots:_vm._u([{key:"header",fn:function(){return [_c('div',{staticClass:"bg-blue-700 h-24 text-white px-4 py-6 rounded-t-lg"},[_c(_vm.getHeaderContent,{tag:"component",attrs:{"show-settings-window":_vm.showSettingsWindow,"object-id":_vm.router.objectId,"object-type":_vm.router.objectType},on:{"update:showSettingsWindow":function($event){_vm.showSettingsWindow=$event},"update:show-settings-window":function($event){_vm.showSettingsWindow=$event},"update:objectId":function($event){return _vm.$set(_vm.router, "objectId", $event)},"update:object-id":function($event){return _vm.$set(_vm.router, "objectId", $event)},"update:objectType":function($event){return _vm.$set(_vm.router, "objectType", $event)},"update:object-type":function($event){return _vm.$set(_vm.router, "objectType", $event)}}})],1)]},proxy:true},{key:"body",fn:function(){return [_c('div',{staticClass:"bg-white max-h-96 text-black px-6 py-4 shadow-md rounded-b-lg overflow-y-auto"},[_c(_vm.getBodyContent,{tag:"component",attrs:{"object-id":_vm.router.objectId,"object-type":_vm.router.objectType},on:{"update:objectId":function($event){return _vm.$set(_vm.router, "objectId", $event)},"update:object-id":function($event){return _vm.$set(_vm.router, "objectId", $event)},"update:objectType":function($event){return _vm.$set(_vm.router, "objectType", $event)},"update:object-type":function($event){return _vm.$set(_vm.router, "objectType", $event)}}})],1)]},proxy:true}],null,false,2539126386)}):_vm._e(),(_vm.showNotificationsWindow)?_c('div',{staticClass:"absolute top-0 left-0 h-screen w-screen z-40",on:{"click":function($event){_vm.showNotificationsWindow = false}}}):_vm._e()],1):_vm._e()}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/NotificationModule.vue?vue&type=template&id=699abe6b&scoped=true&shadow

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"648b734d-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/NotificationCard.vue?vue&type=template&id=05d75f79&scoped=true&
var NotificationWindowvue_type_template_id_05d75f79_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"fixed w-full md:w-80 top-20 right-0 md:right-8 px-2 md:px-0 pt-4 z-50"},[_vm._t("header"),_vm._t("body")],2)}
var NotificationWindowvue_type_template_id_05d75f79_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/NotificationCard.vue?vue&type=template&id=05d75f79&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/NotificationCard.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//


/* harmony default export */ var NotificationWindowvue_type_script_lang_js_ = ({
  name: "NotificationWindow",
  props : {

  },
  data() {
    return {

    }
  },
  computed: {

  },
  methods: {

  }
});

// CONCATENATED MODULE: ./src/components/NotificationCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_NotificationWindowvue_type_script_lang_js_ = (NotificationWindowvue_type_script_lang_js_);
// CONCATENATED MODULE: ./src/components/NotificationCard.vue



function injectStyles (context) {


}

/* normalize component */

var component = normalizeComponent(
  components_NotificationWindowvue_type_script_lang_js_,
  NotificationWindowvue_type_template_id_05d75f79_scoped_true_render,
  NotificationWindowvue_type_template_id_05d75f79_scoped_true_staticRenderFns,
  false,
  injectStyles,
  "05d75f79",
  null
  ,true
)

/* harmony default export */ var NotificationWindow = (component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"648b734d-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/NotificationCenter.vue?vue&type=template&id=818bc0c6&scoped=true&
var FloatingButtonvue_type_template_id_818bc0c6_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{staticClass:"fixed flex w-12 h-12 z-50 right-2 md:right-8 top-8 items-center justify-center rounded-full focus:outline-none bg-blue-700 text-white",on:{"click":function($event){_vm.showNotifications = !_vm.showNotifications}}},[_c('img',_vm._b({},'img',_vm.dynamicWidgetIcon,false)),(_vm.newNotificationsCount && !_vm.showNotifications)?_c('span',{staticClass:"absolute flex h-5 w-5 top-0 right-0 -mt-1 -mr-1"},[_c('span',{staticClass:"relative inline-flex h-5 w-5 justify-center items-center rounded-full bg-red-700 text-xs text-white"},[_vm._v(" "+_vm._s(_vm.newNotificationsCount)+" ")])]):_vm._e()])}
var FloatingButtonvue_type_template_id_818bc0c6_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/NotificationCenter.vue?vue&type=template&id=818bc0c6&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/NotificationCenter.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var FloatingButtonvue_type_script_lang_js_ = ({
  name: "FloatingButton",
  props: {
    showNotificationsWindow: {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      showNotifications: false
    }
  },
  mounted() {
    this.showNotifications = this.showNotificationsWindow
  },
  computed: {

    newNotificationsCount() {
      return this.$store.getters.newNotifications.length
    },

    dynamicWidgetIcon() {
      return {
        src: this.showNotifications
          ? __webpack_require__("5ed1")
          : __webpack_require__("cddb"),
        alt: 'Notifications'
      }
    },

  },
  watch: {
    showNotifications: function() {
      this.$emit('update:showNotificationsWindow', this.showNotifications)
    },
    showNotificationsWindow: function(value) {
      this.showNotifications = value
    }
  },
});

// CONCATENATED MODULE: ./src/components/NotificationCenter.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_FloatingButtonvue_type_script_lang_js_ = (FloatingButtonvue_type_script_lang_js_);
// CONCATENATED MODULE: ./src/components/NotificationCenter.vue



function FloatingButton_injectStyles (context) {


}

/* normalize component */

var FloatingButton_component = normalizeComponent(
  components_FloatingButtonvue_type_script_lang_js_,
  FloatingButtonvue_type_template_id_818bc0c6_scoped_true_render,
  FloatingButtonvue_type_template_id_818bc0c6_scoped_true_staticRenderFns,
  false,
  FloatingButton_injectStyles,
  "818bc0c6",
  null
  ,true
)

/* harmony default export */ var FloatingButton = (FloatingButton_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"648b734d-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/BodyContent/EventGroupPreferencesListHeader.vue?vue&type=template&id=06d3f0cf&scoped=true&
var ChannelSettingsListvue_type_template_id_06d3f0cf_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('h5',{staticClass:"mb-4 text-sm font-bold"},[_vm._v(_vm._s(_vm.channel.title)+" settings")]),_c('div',{class:{'rounded-lg border border-gray-300 px-4':!_vm.activePreferenceId}},_vm._l((_vm.channel.preferences),function(preference,index){return _c('div',{key:preference.id,staticClass:"py-2 border-gray-300",class:{
        'px-4 rounded-lg border mb-4':_vm.activePreferenceId === preference.id && index === 0,
        'px-4 rounded-lg border mt-4':_vm.activePreferenceId === preference.id && index === _vm.channel.preferences.length-1,
        'px-4 rounded-lg border my-4':_vm.activePreferenceId === preference.id && index > 0 && _vm.channel.preferences.length-1 !== index,
        'px-4 rounded-lg border':_vm.activePreferenceId,
        'border-b':!_vm.activePreferenceId,
      }},[_c('div',{staticClass:"flex justify-between items-center cursor-pointer",class:{'border-b pb-2':_vm.activePreferenceId === preference.id},on:{"click":function($event){return _vm.showOptionsList(preference.id)}}},[_c('span',{staticClass:"text-sm font-medium"},[_vm._v(_vm._s(preference.title))]),_c('img',_vm._b({},'img',_vm.dynamicListIcon(preference.id),false))]),(_vm.activePreferenceId === preference.id)?_c('div',{staticClass:"cursor-default"},_vm._l((preference.options),function(option){return _c('div',{key:option.id,staticClass:"flex justify-between items-center mt-2"},[_c('span',{staticClass:"text-sm font-normal"},[_vm._v(_vm._s(option.title))]),_c('toggle',{attrs:{"value":option.value},on:{"update":function($event){return _vm.updateOptionValue($event, _vm.channel.id, preference.id, option.id)}}})],1)}),0):_vm._e()])}),0)])}
var ChannelSettingsListvue_type_template_id_06d3f0cf_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/BodyContent/EventGroupPreferencesListHeader.vue?vue&type=template&id=06d3f0cf&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"648b734d-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Toggle.vue?vue&type=template&id=3bd10818&scoped=true&
var Togglevue_type_template_id_3bd10818_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('label',{staticClass:"toggle"},[_c('input',{attrs:{"type":"checkbox"},domProps:{"checked":_vm.value},on:{"click":function($event){return _vm.$emit('update', !_vm.value)}}}),_c('span',{staticClass:"slider"})])}
var Togglevue_type_template_id_3bd10818_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Toggle.vue?vue&type=template&id=3bd10818&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Toggle.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//

/* harmony default export */ var Togglevue_type_script_lang_js_ = ({
  name: "Toggle",
  props: {
    value : {
      type: Boolean,
      required: true
    }
  }
});

// CONCATENATED MODULE: ./src/components/Toggle.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_Togglevue_type_script_lang_js_ = (Togglevue_type_script_lang_js_);
// CONCATENATED MODULE: ./src/components/Toggle.vue



function Toggle_injectStyles (context) {

  var style0 = __webpack_require__("5370")
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var Toggle_component = normalizeComponent(
  components_Togglevue_type_script_lang_js_,
  Togglevue_type_template_id_3bd10818_scoped_true_render,
  Togglevue_type_template_id_3bd10818_scoped_true_staticRenderFns,
  false,
  Toggle_injectStyles,
  "3bd10818",
  null
  ,true
)

/* harmony default export */ var Toggle = (Toggle_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/BodyContent/EventGroupPreferencesListHeader.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var ChannelSettingsListvue_type_script_lang_js_ = ({
  name: "ChannelSettingsList",
  components: { Toggle: Toggle },
  props: {
    objectId : {
      type: Number,
      required: true
    }
  },
  data() {
    return {
      activePreferenceId: null
    }
  },
  computed: {
    channel() {
      return this.$store.getters.getSettingById(this.objectId)
    },

  },
  methods: {
    dynamicListIcon(id) {
      return {
        src: this.activePreferenceId !== id
          ? __webpack_require__("10a6")
          : __webpack_require__("8c6d"),
        alt: 'Arrow'
      }
    },
    showOptionsList(id) {
      if (this.activePreferenceId === id) {
        this.activePreferenceId = null
      } else {
        this.activePreferenceId = id
      }
    },
    updateOptionValue(newValue, channelId, preferenceId, optionId) {
      this.$store.commit('updateSettings', {
        channelId: channelId,
        optionId: optionId,
        preferenceId: preferenceId,
        value: newValue
      })
    }
  }
});

// CONCATENATED MODULE: ./src/components/BodyContent/EventGroupPreferencesListHeader.vue?vue&type=script&lang=js&
 /* harmony default export */ var BodyContent_ChannelSettingsListvue_type_script_lang_js_ = (ChannelSettingsListvue_type_script_lang_js_);
// CONCATENATED MODULE: ./src/components/BodyContent/EventGroupPreferencesListHeader.vue



function ChannelSettingsList_injectStyles (context) {


}

/* normalize component */

var ChannelSettingsList_component = normalizeComponent(
  BodyContent_ChannelSettingsListvue_type_script_lang_js_,
  ChannelSettingsListvue_type_template_id_06d3f0cf_scoped_true_render,
  ChannelSettingsListvue_type_template_id_06d3f0cf_scoped_true_staticRenderFns,
  false,
  ChannelSettingsList_injectStyles,
  "06d3f0cf",
  null
  ,true
)

/* harmony default export */ var ChannelSettingsList = (ChannelSettingsList_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"648b734d-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/BodyContent/NotificationsListHeader.vue?vue&type=template&id=5c100733&scoped=true&
var NotificationsListvue_type_template_id_5c100733_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.notificationsCount)?_c('div',[(_vm.newNotifications.length)?_c('div',{staticClass:"mb-4"},[_c('h5',{staticClass:"mb-4 text-sm font-bold"},[_vm._v("New notifications")]),_vm._l((_vm.newNotifications),function(notification){return _c('div',{key:notification.id,staticClass:"hover:bg-gray-200 rounded-lg -m-2 p-2 mb-2 cursor-pointer"},[_c('span',{staticClass:"text-sm block"},[_vm._v(_vm._s(notification.title))]),_c('div',{staticClass:"flex justify-between items-center"},[_c('div',[_c('span',{staticClass:"text-xs text-blue-700 font-bold"},[_vm._v(_vm._s(notification.created_at))]),_c('span',{staticClass:"text-xs text-gray-500"},[_vm._v(" - "+_vm._s(notification.type))])]),_c('div',{staticClass:"h-2 w-2 rounded-full bg-blue-700"})])])})],2):_vm._e(),(_vm.readNotifications.length)?_c('div',[_c('h5',{staticClass:"mb-4 text-sm font-bold"},[_vm._v("Read notifications")]),_vm._l((_vm.readNotifications),function(notification){return _c('div',{key:notification.id,staticClass:"hover:bg-gray-200 rounded-lg -m-2 p-2 mb-2 cursor-pointer"},[_c('span',{staticClass:"text-sm block"},[_vm._v(_vm._s(notification.title))]),_c('span',{staticClass:"text-xs text-gray-500 font-bold"},[_vm._v(_vm._s(notification.created_at))]),_c('span',{staticClass:"text-xs text-gray-500"},[_vm._v(" - "+_vm._s(notification.type))])])})],2):_vm._e()]):_c('div',{staticClass:"flex flex-col min-h-full justify-center items-center mt-2"},[_c('img',{attrs:{"src":__webpack_require__("3075"),"alt":"Empty"}}),_c('span',{staticClass:"mt-4 text-sm text-gray-500"},[_vm._v("Your notifications are empty")])])}
var NotificationsListvue_type_template_id_5c100733_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/BodyContent/NotificationsListHeader.vue?vue&type=template&id=5c100733&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/BodyContent/NotificationsListHeader.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var NotificationsListvue_type_script_lang_js_ = ({
  name: "NotificationsList",
  data() {
    return {

    }
  },

  computed: {
    notificationsCount() {
      return this.$store.getters.notifications.length
    },

    newNotifications() {
      return this.$store.getters.newNotifications
    },

    readNotifications() {
      return this.$store.getters.readNotifications
    },
  },
  methods: {

  }
});

// CONCATENATED MODULE: ./src/components/BodyContent/NotificationsListHeader.vue?vue&type=script&lang=js&
 /* harmony default export */ var BodyContent_NotificationsListvue_type_script_lang_js_ = (NotificationsListvue_type_script_lang_js_);
// CONCATENATED MODULE: ./src/components/BodyContent/NotificationsListHeader.vue



function NotificationsList_injectStyles (context) {


}

/* normalize component */

var NotificationsList_component = normalizeComponent(
  BodyContent_NotificationsListvue_type_script_lang_js_,
  NotificationsListvue_type_template_id_5c100733_scoped_true_render,
  NotificationsListvue_type_template_id_5c100733_scoped_true_staticRenderFns,
  false,
  NotificationsList_injectStyles,
  "5c100733",
  null
  ,true
)

/* harmony default export */ var NotificationsList = (NotificationsList_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"648b734d-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/BodyContent/EventGroupsListHeader.vue?vue&type=template&id=ae7a966c&scoped=true&
var ChannelsListvue_type_template_id_ae7a966c_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('h5',{staticClass:"mb-4 text-sm font-bold"},[_vm._v("Select a category to make changes")]),_vm._l((_vm.channels),function(channel){return _c('div',{key:channel.id,staticClass:"hover:bg-gray-200 rounded-lg -m-2 p-2 mb-2 cursor-pointer",on:{"click":function($event){_vm.channelId = channel.id}}},[_c('div',{staticClass:"flex justify-between items-center"},[_c('span',{staticClass:"text-sm"},[_vm._v(_vm._s(channel.title))]),_c('img',{attrs:{"src":__webpack_require__("81f2"),"alt":"Arrow"}})])])})],2)}
var ChannelsListvue_type_template_id_ae7a966c_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/BodyContent/EventGroupsListHeader.vue?vue&type=template&id=ae7a966c&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/BodyContent/EventGroupsListHeader.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var ChannelsListvue_type_script_lang_js_ = ({
  name: "ChannelsList",
  data() {
    return {
      channelId: null
    }
  },
  computed: {

    channels() {
      return this.$store.getters.settings
    },

  },
  watch: {
    channelId: function() {
      this.$emit('update:objectId', this.channelId)
      this.$emit('update:objectType', 'channel')
    },
  },
  methods: {

  }
});

// CONCATENATED MODULE: ./src/components/BodyContent/EventGroupsListHeader.vue?vue&type=script&lang=js&
 /* harmony default export */ var BodyContent_ChannelsListvue_type_script_lang_js_ = (ChannelsListvue_type_script_lang_js_);
// CONCATENATED MODULE: ./src/components/BodyContent/EventGroupsListHeader.vue



function ChannelsList_injectStyles (context) {


}

/* normalize component */

var ChannelsList_component = normalizeComponent(
  BodyContent_ChannelsListvue_type_script_lang_js_,
  ChannelsListvue_type_template_id_ae7a966c_scoped_true_render,
  ChannelsListvue_type_template_id_ae7a966c_scoped_true_staticRenderFns,
  false,
  ChannelsList_injectStyles,
  "ae7a966c",
  null
  ,true
)

/* harmony default export */ var ChannelsList = (ChannelsList_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"648b734d-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/HeaderContent/NotificationsListHeader.vue?vue&type=template&id=5181644a&scoped=true&
var NotificationsListvue_type_template_id_5181644a_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"relative"},[_c('div',{staticClass:"flex justify-between"},[_c('div',{staticClass:"inline-flex"},[_c('img',{staticClass:"h-10 w-10 mr-4",attrs:{"src":__webpack_require__("2919"),"alt":"Notifications"}}),_vm._m(0)]),_c('img',{staticClass:"cursor-pointer w-6 h-6",attrs:{"src":__webpack_require__("130b"),"alt":"Settings"},on:{"click":function($event){_vm.showHeaderOptionsMenu = !_vm.showHeaderOptionsMenu}}})]),(_vm.showHeaderOptionsMenu)?_c('div',{staticClass:"absolute w-full md:w-72 max-h-24 mt-8 top-0 rounded-lg p-2 bg-white text-black text-sm shadow-md"},[(_vm.newNotificationsCount)?_c('div',{staticClass:"flex items-center hover:bg-gray-200 rounded-lg p-2 cursor-pointer",on:{"click":function($event){_vm.markAllAsRead(); _vm.showHeaderOptionsMenu = false}}},[_c('img',{staticClass:"mr-2",attrs:{"src":__webpack_require__("0dc8"),"alt":"Check"}}),_c('span',[_vm._v("Mark all as read")])]):_vm._e(),_c('div',{staticClass:"flex items-center hover:bg-gray-200 rounded-lg p-2 cursor-pointer",on:{"click":function($event){_vm.showSettings = ! _vm.showSettings; _vm.showHeaderOptionsMenu = false}}},[_c('img',{staticClass:"mr-2",attrs:{"src":__webpack_require__("87d1"),"alt":"Cog"}}),_c('span',[_vm._v("Settings menu")])])]):_vm._e()])}
var NotificationsListvue_type_template_id_5181644a_scoped_true_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('h4',{staticClass:"text-base font-bold"},[_vm._v("Notifications")]),_c('span',{staticClass:"text-xs"},[_vm._v("View and manage your notifications")])])}]


// CONCATENATED MODULE: ./src/components/HeaderContent/NotificationsListHeader.vue?vue&type=template&id=5181644a&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/HeaderContent/NotificationsListHeader.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var HeaderContent_NotificationsListvue_type_script_lang_js_ = ({
  name: "NotificationsList",
  props: {
    showSettingsWindow : {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      showHeaderOptionsMenu: false,
      showSettings: false
    }
  },
  mounted() {
    this.showSettings = this.showSettingsWindow
  },
  computed: {
    newNotificationsCount() {
      return this.$store.getters.newNotifications.length
    }
  },
  watch: {
    showSettings: function() {
      this.$emit('update:showSettingsWindow', this.showSettings)
    },
  },
  methods: {
    markAllAsRead() {
      this.$store.commit('markAsReadAll')
    }
  }
});

// CONCATENATED MODULE: ./src/components/HeaderContent/NotificationsListHeader.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_HeaderContent_NotificationsListvue_type_script_lang_js_ = (HeaderContent_NotificationsListvue_type_script_lang_js_);
// CONCATENATED MODULE: ./src/components/HeaderContent/NotificationsListHeader.vue



function HeaderContent_NotificationsList_injectStyles (context) {


}

/* normalize component */

var HeaderContent_NotificationsList_component = normalizeComponent(
  components_HeaderContent_NotificationsListvue_type_script_lang_js_,
  NotificationsListvue_type_template_id_5181644a_scoped_true_render,
  NotificationsListvue_type_template_id_5181644a_scoped_true_staticRenderFns,
  false,
  HeaderContent_NotificationsList_injectStyles,
  "5181644a",
  null
  ,true
)

/* harmony default export */ var HeaderContent_NotificationsList = (HeaderContent_NotificationsList_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"648b734d-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/HeaderContent/EventGroupsListHeader.vue?vue&type=template&id=4418e102&scoped=true&
var ChannelsListvue_type_template_id_4418e102_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"flex items-center"},[_c('div',{staticClass:"mr-4"},[_c('img',{staticClass:"cursor-pointer",attrs:{"src":__webpack_require__("f9df"),"alt":"Arrow"},on:{"click":function($event){_vm.showSettings = ! _vm.showSettings}}})]),_vm._m(0)])}
var ChannelsListvue_type_template_id_4418e102_scoped_true_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('h4',{staticClass:"text-base font-bold"},[_vm._v("Notification Settings")]),_c('span',{staticClass:"text-xs"},[_vm._v("Manage and change your notifications")])])}]


// CONCATENATED MODULE: ./src/components/HeaderContent/EventGroupsListHeader.vue?vue&type=template&id=4418e102&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/HeaderContent/EventGroupsListHeader.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var HeaderContent_ChannelsListvue_type_script_lang_js_ = ({
  name: "ChannelsList",
  props: {
    showSettingsWindow : {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      showSettings: false
    }
  },
  mounted() {
    this.showSettings = this.showSettingsWindow
  },
  computed: {

  },
  watch: {
    showSettings: function() {
      this.$emit('update:showSettingsWindow', this.showSettings)
    },
  },
  methods: {

  }
});

// CONCATENATED MODULE: ./src/components/HeaderContent/EventGroupsListHeader.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_HeaderContent_ChannelsListvue_type_script_lang_js_ = (HeaderContent_ChannelsListvue_type_script_lang_js_);
// CONCATENATED MODULE: ./src/components/HeaderContent/EventGroupsListHeader.vue



function HeaderContent_ChannelsList_injectStyles (context) {


}

/* normalize component */

var HeaderContent_ChannelsList_component = normalizeComponent(
  components_HeaderContent_ChannelsListvue_type_script_lang_js_,
  ChannelsListvue_type_template_id_4418e102_scoped_true_render,
  ChannelsListvue_type_template_id_4418e102_scoped_true_staticRenderFns,
  false,
  HeaderContent_ChannelsList_injectStyles,
  "4418e102",
  null
  ,true
)

/* harmony default export */ var HeaderContent_ChannelsList = (HeaderContent_ChannelsList_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"648b734d-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/HeaderContent/EventGroupPreferencesListHeader.vue?vue&type=template&id=6fce7b44&scoped=true&
var ChannelSettingsListvue_type_template_id_6fce7b44_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"flex items-center"},[_c('div',{staticClass:"mr-4"},[_c('img',{staticClass:"cursor-pointer",attrs:{"src":__webpack_require__("f9df"),"alt":"Arrow"},on:{"click":function($event){return _vm.$emit('update:objectId', null)}}})]),_vm._m(0)])}
var ChannelSettingsListvue_type_template_id_6fce7b44_scoped_true_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('h4',{staticClass:"text-base font-bold"},[_vm._v("Notification Settings")]),_c('span',{staticClass:"text-xs"},[_vm._v("Manage and change your notifications")])])}]


// CONCATENATED MODULE: ./src/components/HeaderContent/EventGroupPreferencesListHeader.vue?vue&type=template&id=6fce7b44&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/HeaderContent/EventGroupPreferencesListHeader.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var HeaderContent_ChannelSettingsListvue_type_script_lang_js_ = ({
  name: "ChannelSettingsList",
  props: {

  },
  data() {
    return {

    }
  },
  mounted() {

  },
  computed: {

  },
  watch: {

  },
  methods: {

  }
});

// CONCATENATED MODULE: ./src/components/HeaderContent/EventGroupPreferencesListHeader.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_HeaderContent_ChannelSettingsListvue_type_script_lang_js_ = (HeaderContent_ChannelSettingsListvue_type_script_lang_js_);
// CONCATENATED MODULE: ./src/components/HeaderContent/EventGroupPreferencesListHeader.vue



function HeaderContent_ChannelSettingsList_injectStyles (context) {


}

/* normalize component */

var HeaderContent_ChannelSettingsList_component = normalizeComponent(
  components_HeaderContent_ChannelSettingsListvue_type_script_lang_js_,
  ChannelSettingsListvue_type_template_id_6fce7b44_scoped_true_render,
  ChannelSettingsListvue_type_template_id_6fce7b44_scoped_true_staticRenderFns,
  false,
  HeaderContent_ChannelSettingsList_injectStyles,
  "6fce7b44",
  null
  ,true
)

/* harmony default export */ var HeaderContent_ChannelSettingsList = (HeaderContent_ChannelSettingsList_component.exports);
// EXTERNAL MODULE: ./node_modules/vuex/dist/vuex.esm.js
var vuex_esm = __webpack_require__("2f62");

// CONCATENATED MODULE: ./src/store/index.js



external_Vue_default.a.use(vuex_esm["a" /* default */])

/* harmony default export */ var store = (new vuex_esm["a" /* default */].Store({
  state: {
    list: [
      {
        id: 1,
        title: "Invoice 00001 was successfully created. Lorem ipsum dolor sit amet",
        type: "Account status",
        created_at: "15 minutes ago",
        is_read: false,
      },
      {
        id: 2,
        title: "You have new message from Xuser.",
        type: "Affiliate request",
        created_at: "5 minutes ago",
        is_read: false,
      },
      {
        id: 3,
        title: "Invoice 00000 was successfully created. Lorem ipsum dolor sit amet",
        type: "Affiliate request",
        created_at: "40 minutes ago",
        is_read: true,
      },
      {
        id: 4,
        title: "You have new message from Yuser.",
        type: "Affiliate request",
        created_at: "1 day ago",
        is_read: true,
      },
    ],
    settings: [
      {
        id: 1,
        title: "Text message",
        preferences: [
          {
            id: 1,
            title: "Message center",
            options: [
              {
                id: 1,
                title: "Message received",
                value: true,
              },
              {
                id: 2,
                title: "Support message received",
                value: false,
              },
            ]
          },
          {
            id: 2,
            title: "Compliance",
            options: [
              {
                id: 3,
                title: "Test setting 1",
                value: false,
              },
              {
                id: 4,
                title: "Test setting 2",
                value: false,
              },
            ]
          },
          {
            id: 3,
            title: "Something",
            options: [
              {
                id: 9,
                title: "Test setting 3",
                value: false,
              },
              {
                id: 10,
                title: "Test setting 4",
                value: false,
              },
            ]
          }
        ],
      },
      {
        id: 2,
        title: "Browser notifications",
        preferences: [
          {
            id: 3,
            title: "Test options 1",
            options: [
              {
                id: 5,
                title: "Test setting 1",
                value: true,
              },
              {
                id: 6,
                title: "Test setting 2",
                value: false,
              },
            ]
          },
          {
            id: 4,
            title: "Test options 2",
            options: [
              {
                id: 7,
                title: "Test setting 1",
                value: true,
              },
              {
                id: 8,
                title: "Test setting 2",
                value: false,
              },
            ]
          }
        ],
      },
    ]
  },
  mutations: {
    markAsReadAll (state) {
      state.list.filter(notification => !notification.is_read)
          .forEach(function (item, index, arr) {
            arr[index].is_read = true
          })
    },
    updateSettings (state , { optionId, preferenceId, channelId, value }) {
      state.settings
          .find(channel => channel.id === channelId).preferences
          .find(preference => preference.id === preferenceId).options
          .find(option => option.id === optionId).value = value
    }
  },
  getters : {
    notifications (state) {
      return state.list
    },
    newNotifications(state) {
      return state.list.filter(notification => !notification.is_read)
    },
    readNotifications(state) {
      return state.list.filter(notification => notification.is_read)
    },
    settings (state) {
      return state.settings
    },
    getSettingById: (state) => id => {
      return state.settings.find(setting => setting.id === id)
    },
  },
  actions: {
  },
  modules: {
  }
}));

// EXTERNAL MODULE: ./src/assets/css/tailwind.css
var tailwind = __webpack_require__("def6");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/NotificationModule.vue?vue&type=script&lang=js&shadow
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//















/* harmony default export */ var NotificationModulevue_type_script_lang_js_shadow = ({
  store: store,
  name: "NotificationModule",
  components: { NotificationWindow: NotificationWindow, FloatingButton: FloatingButton },
  data() {
    return {
      moduleIsReady: false,
      showSettingsWindow: false,
      showNotificationsWindow: false,
      router : {
        objectId: null,
        objectType: null
      }
    }
  },
  created() {
    this.bootstrap();
  },
  computed: {

    getHeaderContent() {
      if (this.router.objectId && this.router.objectType === 'channel') {
        return HeaderContent_ChannelSettingsList
      } else if (this.showSettingsWindow) {
        return HeaderContent_ChannelsList
      } else {
        return HeaderContent_NotificationsList
      }
    },
    getBodyContent() {
       if (this.router.objectId && this.router.objectType === 'channel') {
        return ChannelSettingsList
      } else if (this.showSettingsWindow) {
         return ChannelsList
       } else {
        return NotificationsList
      }
    },
  },
  watch: {
    showNotificationsWindow: function () {
      this.showSettingsWindow = false
      this.router.objectId = null
      this.router.objectType = null
    },
    showSettingsWindow: function () {
      this.router.objectId = null
    }
  },
  methods: {

    async bootstrap() {

      //imitate some data loading from api
      // let loading = new Promise((resolve, reject) => {
      //   setTimeout(() => {
      //     resolve();
      //   }, 300);
      // });
      //
      // await loading.then(() =>
      //   this.moduleIsReady = true
      // )
      this.moduleIsReady = true
    }
  }
});

// CONCATENATED MODULE: ./src/NotificationModule.vue?vue&type=script&lang=js&shadow
 /* harmony default export */ var src_NotificationModulevue_type_script_lang_js_shadow = (NotificationModulevue_type_script_lang_js_shadow);
// CONCATENATED MODULE: ./src/NotificationModule.vue?shadow



function NotificationModuleshadow_injectStyles (context) {

  var style0 = __webpack_require__("8b78")
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var NotificationModuleshadow_component = normalizeComponent(
  src_NotificationModulevue_type_script_lang_js_shadow,
  render,
  staticRenderFns,
  false,
  NotificationModuleshadow_injectStyles,
  "699abe6b",
  null
  ,true
)

/* harmony default export */ var NotificationModuleshadow = (NotificationModuleshadow_component.exports);
// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-wc.js




// runtime shared by every component chunk





window.customElements.define('notification-widget', vue_wc_wrapper(external_Vue_default.a, NotificationModuleshadow))

/***/ }),

/***/ "5ed1":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/arrow_bottom_white.a2b5a8b3.svg";

/***/ }),

/***/ "81f2":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/arrow_right_black.ef7a9784.svg";

/***/ }),

/***/ "87d1":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/cog_icon.7513b1f5.svg";

/***/ }),

/***/ "8875":
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;// addapted from the document.currentScript polyfill by Adam Miller
// MIT license
// source: https://github.com/amiller-gh/currentScript-polyfill

// added support for Firefox https://bugzilla.mozilla.org/show_bug.cgi?id=1620505

(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
}(typeof self !== 'undefined' ? self : this, function () {
  function getCurrentScript () {
    var descriptor = Object.getOwnPropertyDescriptor(document, 'currentScript')
    // for chrome
    if (!descriptor && 'currentScript' in document && document.currentScript) {
      return document.currentScript
    }

    // for other browsers with native support for currentScript
    if (descriptor && descriptor.get !== getCurrentScript && document.currentScript) {
      return document.currentScript
    }

    // IE 8-10 support script readyState
    // IE 11+ & Firefox support stack trace
    try {
      throw new Error();
    }
    catch (err) {
      // Find the second match for the "at" string to get file src url from stack.
      var ieStackRegExp = /.*at [^(]*\((.*):(.+):(.+)\)$/ig,
        ffStackRegExp = /@([^@]*):(\d+):(\d+)\s*$/ig,
        stackDetails = ieStackRegExp.exec(err.stack) || ffStackRegExp.exec(err.stack),
        scriptLocation = (stackDetails && stackDetails[1]) || false,
        line = (stackDetails && stackDetails[2]) || false,
        currentLocation = document.location.href.replace(document.location.hash, ''),
        pageSource,
        inlineScriptSourceRegExp,
        inlineScriptSource,
        scripts = document.getElementsByTagName('script'); // Live NodeList collection

      if (scriptLocation === currentLocation) {
        pageSource = document.documentElement.outerHTML;
        inlineScriptSourceRegExp = new RegExp('(?:[^\\n]+?\\n){0,' + (line - 2) + '}[^<]*<script>([\\d\\D]*?)<\\/script>[\\d\\D]*', 'i');
        inlineScriptSource = pageSource.replace(inlineScriptSourceRegExp, '$1').trim();
      }

      for (var i = 0; i < scripts.length; i++) {
        // If ready state is interactive, return the script tag
        if (scripts[i].readyState === 'interactive') {
          return scripts[i];
        }

        // If src matches, return the script tag
        if (scripts[i].src === scriptLocation) {
          return scripts[i];
        }

        // If inline source matches, return the script tag
        if (
          scriptLocation === currentLocation &&
          scripts[i].innerHTML &&
          scripts[i].innerHTML.trim() === inlineScriptSource
        ) {
          return scripts[i];
        }
      }

      // If no match, return null
      return null;
    }
  };

  return getCurrentScript
}));


/***/ }),

/***/ "8b78":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationModule_vue_vue_type_style_index_0_id_699abe6b_scoped_true_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("4ce9");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationModule_vue_vue_type_style_index_0_id_699abe6b_scoped_true_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationModule_vue_vue_type_style_index_0_id_699abe6b_scoped_true_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationModule_vue_vue_type_style_index_0_id_699abe6b_scoped_true_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationModule_vue_vue_type_style_index_0_id_699abe6b_scoped_true_lang_css_shadow__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "8bbf":
/***/ (function(module, exports) {

module.exports = Vue;

/***/ }),

/***/ "8c6d":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/arrow_top_black.eba2ac7b.svg";

/***/ }),

/***/ "baf0":
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__("24fb");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".toggle[data-v-3bd10818]{position:relative;display:inline-block;width:30px;height:16px}.toggle input[data-v-3bd10818]{opacity:0;width:0;height:0}.slider[data-v-3bd10818]{cursor:pointer;top:0;left:0;right:0;bottom:0;background-color:#d1d5db;border-radius:10px}.slider[data-v-3bd10818],.slider[data-v-3bd10818]:before{position:absolute;transition:.4s}.slider[data-v-3bd10818]:before{content:\"\";height:14px;width:14px;left:2px;bottom:1px;background-color:#fff;border-radius:50%}input:checked+.slider[data-v-3bd10818]{background-color:#2563eb}input:focus+.slider[data-v-3bd10818]{box-shadow:0 0 1px #2563eb}input:checked+.slider[data-v-3bd10818]:before{transform:translateX(13px)}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "c8ba":
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "cddb":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/bell_square_white.1849fba6.svg";

/***/ }),

/***/ "def6":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("497d");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to Shadow Root
var add = __webpack_require__("35d6").default
module.exports.__inject__ = function (shadowRoot) {
  add("54629c93", content, shadowRoot)
};

/***/ }),

/***/ "f9df":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/arrow_left_white.859b19b5.svg";

/***/ })

/******/ });
//# sourceMappingURL=notification-widget.js.map
